# FAQ formations informatiques et session d'examen

Dans cette session, les questions ont essentiellement tourné autour de la formation **DWWM** (**D**éveloppeur **W**eb et **W**eb **M**obile). Il s'agit d'une formation répandue, qui dure en général entre 6 et 12 mois (stage compris), et qui est accessible à des personnes peu ou pas diplômées.

Afin que ce document soit utile à toutes et tous, je ne liste ici que les questions générales qui ne sont pas propres aux projets qui ont été présentés/discutés.

## Les questions

+++question "J'ai l'habitude de travailler en autonomie, autrement dit, je ne suis pas attiré par le travail en équipe. Est-ce que ça peut poser probleme pour la validation du titre ?"

- un jury a environ 1h30 pour se faire un avis sur un candidat, c'est très peu et il faut limiter alors limiter au possible les signes négatifs. Il est tout à fait possible d'exprimer sa préférence pour le travail en autonomie, mais il ne faut pas fermer la porte au travail en équipe. Le jury ne sait pas de quoi est fait demain, vous non plus ;) 
- posez vous aussi la question de votre rapport avec le travail en équipe. Il existe bien sur des projets nécessitant peu de développeurs, mais vous vous coupez d'une part importante d'opportunités professionnelles qui se présenteront à vous. De plus, vous serez vu comme ayant un niveau junior dans votre premier vrai emploi (même avec le meilleur entretien du monde), vous serez très probablement amenés à travailler avec une autre personne qui devra s'assurer de votre bon intégration mais aussi que vous tenez bien la route, techniquement parlant.

+++question "J'ai du mal à obtenir des avis sur le projet que je souhaite présenter lors de mon passage en examen"

- partager le repository github/gitlab/... du projet <a id="discord" href="https://discord.gg/y2Cp8MRw" target="_blank">sur le serveur discord</a>, d'autres personnes sur le serveur (parfois dans la même situation que vous) pourront l'analyser et faire des retours. 
- les projets peuvent être aussi analysés durant les lives twitch (avec l'accord de la personne qui a fait le projet)
    - ce qui a été fait d'ailleurs durant cette session twitch

+++question "Est-ce grave si je ne respecte pas le temps de présentation qui est prévu ?"

- **Si sur les 30mn de présentation il vous manque plus de 5mn**, cela risque de donner l'impression que le projet présenté est trop léger et/ou mal préparé. Voici différentes pistes pour étoffer sa présentation : 
    - rajouter une fonctionnalité en rapport avec les compétences du titre
    - prenez plus de temps sur les parties dans le code qui vous ont donnée des difficulté. On (le jury) sait que vous avez galéré sur certaines partie du code, c'est très intéressant pour vous de montrer au jury la démarche que vous avez lorsque vous avez fait face à un problème. C'est de plus une occasion pour vous de parler de site que vous consulter, de mettre en avant l'anglais que vous avez améliorés avec le temps, etc...

- **Si sur les 30mn de présentation vous dépassez le temps prévu (plus de 5mn disons)**. Faîtes attention également : le fait d'avoir beaucoup de choses à présenter peut avoir un effet rassurant pour vous, **MAIS**, cela peut envoyer des mauvais signaux au jury : 
    - est-ce que ce candidat a réellement préparé sa présentation ? 
    - la partie X n'est pas vraiment essentielle ni en lien avec les compétences du titre, pour quelle raison a-t'il prit du temps dessus ? 
    - etc

+++bulle spacecodeur
  faire la synthèse de son travail pour le présenter en des temps impartis est un travail qu'il ne faut pas négliger
+++

+++question "Je n'ai pas pu produire un projet consistant durant mon stage, ça me semble compliqué de le présenter pour l'examen"

- les jurys ont l'habitude d'auditionner des candidats sur des projets hors stage. Différentes raisons peuvent expliquer légitimement que le résultat produit du stage ne puisse être présenté. 
- toutefois, si vous présentez des projets effectués pendant la formation et/ou sur votre temps libre, mettez en avant les aspect professionnels de votre démarche. Le jury doit avoir les clés en main pour pouvoir se dire "cette personne là est professionnelle et sérieuse"

+++question "Comment doit on structurer notre rapport ?"

- Je préconise la structure suivante : elle est légère et elle facilite à la fois votre travail et celui du jury
    - consacrez une première partie qui présente de manière générale votre ou vos projets.
    - consacrez une deuxieme partie qui suit directement la structure des compétences du titre, ainsi : 
        - le jury peut trouver facilement là où les compétences à valider ont été travaillées
        - cela montre au jury que vous êtes prêt à vous défendre sur chacune des compétences
        - cela vous permet aussi, pendant la rédaction de votre rapport, d'identifier facilement et en amont les points éventuellement faibles sur lesquels un jury attentif ne manquera pas de poser le doigt pendant l'examen

+++bulle spacecodeur
  attention ! le rapport doit faire un certain nombre de page avec un nombre de caractères total minimal. Vous trouverez ces informations dans le document "<b>R</b>éférentiel de <b>C</b>ertification du <b>T</b>itre <b>P</b>rofessionnel"
+++

---

+++bulle spacecodeur
  pour terminer, quelques tips donnés pendant le live
+++

+++bulle spacecodeur droite
  numérotez vos slides de présentation, c'est un confort important pour le jury
+++

+++bulle spacecodeur droite
  faîtes relire votre rapport et vos slides. Les fautes d'orthographes, surtout montrées sur un grand écran, ça pique les yeux !
+++

+++bulle spacecodeur droite
  présentez vous dans une tenue correcte, il n'est pas nécessaire de sortir la cravate ou le tailleur, par contre évitez les joggings par exemple ;)
+++

+++bulle spacecodeur droite
  le jury sait que l'examen est sujet à stress, surtout pour certains candidats en reconvertion professionnelle. N'hésitez pas à prendre quelques secondes pour en parler avec le jury au début de la session. Ce dernier devrait trouver de bons mots pour vous rassurer
+++