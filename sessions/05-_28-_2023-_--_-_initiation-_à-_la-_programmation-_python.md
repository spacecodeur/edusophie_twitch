# Initiation à la programmation python

Durant ce live, une initiation à la programmation python a été faîte. Ce document a été créé durant le live.

+++sommaire

## Petite session initiation en programmation

### Stocker et manipuler un nombre

widget python

a = 10
b = -1

print(a + b)

widget

### Stocker et manipuler une chaine de caractères

widget python

age = 27
prenom = "toto"

print(prenom)
print(age)

widget

`prenom = "toto"` revient à dire `ma machine, s'il te plait, stoque temporairement la valeur "toto" dans la case mémoire de ta RAM que je nomme 'prenom'`

+++image "/images/md/2FDHD4F747GE7C283H9AC4E2AE4BB2" col-10 mx-auto

Cette mémoire est gérée par la RAM d'un ordinateur par exemple

widget python

# je veux afficher 'toto a 27 ans'

age = "27"
prenom = "toto"

print(prenom + ' a ' + age + ' ans')

widget

### Simuler le code utilisé par un bot discord


widget python

# simuler un bot qui affiche un message d'alerte si un utilisateur écrit tout en majuscule

messageUtilisateur = "KIKOO SA VA"

if(messageUtilisateur.isupper()):
  print("Hey ! tu n'utilises que des majuscules, c'est pas cool !")
else:
  print("")

widget

### Le typage des données 

widget python

nombre1 = 50 # on a une variable de type entier (int)
nombre2 = 30 # on a une variable de type entier (int)

objet = "table" # on a une variable de type "chaine de caracteres" (string)

print(nombre1 - nombre2) # affiche 20
print(nombre1 - objet) # affiche une erreur car nombre1 et objet ne sont pas du meme type

widget

#### Erreur lorsque les types sont différents, comment faire ? 

widget python

pv = 42
pseudo = 'okli'

pvQuiEstUneCHaineDeCaractere = str(pv) # str convertit un entier en chaine de caracteres

print(pseudo + " a " + pvQuiEstUneCHaineDeCaractere + " points de vie")

widget


### Exemple jeu calcul mental 

widget python
import random

a = random.randint(2,10)
b = random.randint(2,10)

retultat = a * b

reponseJoueur = input("combien font " + str(a) + " * " + str(b) + " ? ")

if(reponseJoueur == str(retultat)):
  print("Bravo ! c'est le bon résultat !")
else:
  print("Tu t'es trompé, le résultat est : " + str(retultat))
  
widget