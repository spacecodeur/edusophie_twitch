# Roadmap <br> Bases en programmation

+++sommaire 1

<a class="supportlink" href="session-_--_-_28-_05-_2023.md" target="_blank">
  <i class="isFile fas fa-file-alt fa-4x"></i>
</a>

+++roadmap
	S'outiller pour programmer
	
	IDE = ?
  <a id="thonny" href="https://thonny.org/" target="_blank">thonny</a>
  Installation

  Afficher un message
  fonction <code>print</code>
  fonction : principes généraux
  les bonnes habitudes : commenter son code
+++

+++roadmap
	Un peu d'informatique fondamentale

  Hardware
  processeur
  mémoire vive
  mémoire morte

  Les langages de programmation
  lien avec le hardware
  histoire générale
  
  Python : histoire et caractéristiques
  histoire générale
  pourquoi choisir python ?
+++

+++roadmap
	Programmation python I

  Des chiffres et des lettres
  comment les stocker ?
  comment les manipuler ?
  composant(s) hardware utilisé(s) ?
  fonction <code>type</code>

  Les ensembles (simples) de données
  à quoi ça sert ?
  comment les stocker ?
  comment les manipuler ?
+++

+++roadmap
	Fichier de données I

  L'Open Data
  histoire générale
  entrepôts de données ouvertes
  téléchargement et analyse d'un fichier de données .csv sur Opendatagouv

  Manipulation (simple) d'un fichier de données avec python
  fonctions <code>open()</code> et <code>read()</code>
  afficher chaque lignes du fichier
  composant(s) hardware utilisé(s) ?
  variable type 'objet' : notions
+++

+++roadmap
	Programmation python II

  Les chaînes de caractère
  petits rappels
  fonction <code>split</code>

  Répéter des bouts de programme
  instruction <code>for</code>
  initiation en algorithmie\
  
  Lire des ensembles de données de 2 dimentions
  petit point vocabulaire : tableau (ou liste) de dimention 2, tableau de tableau, matrice 
+++

+++roadmap
  Fichier de données II
  
  Manipulation d'un fichier de données avec python
  rappels
  afficher chaque information contenue dans chaque ligne
+++

+++roadmap
  Programmation python III
  
  Les variables booléennes
  les booléens, pourquoi faire ?
  histoire générale
  tables de Bool
  
  Utiliser des conditions dans son programme
  instruction <code>if</code>
  instruction <code>else</code>
  instruction <code>elif</code>
  
  Codons un p'tit jeu
  on choisis ensemble les règles
+++

+++roadmap
  Projet data science I
  
  Présentation du sujet
  lister les 10 prénoms les plus courants en France
  
  Récupération du fichier de données
  
  Place au code !
  étapes principales
  à nos claviers !
+++

+++roadmap
  Programmation python IV
  
  Manipulation d'un fichier de données
  supprimer l'entête
  instruction <code>in</code>
  
  Les dictionnaires en python
  les dictionnaires, pourquoi faire ?
  petit point vocabulaire : dictionnaire, tableau associatif 
  
  Un peu de pratique
  compter le nombre de doublons dans une liste
  afficher le résultat dans un dictionnaire
+++

+++roadmap
  Projet pratique
  
  Présentation du sujet
  créer un mini-correcteur orthographique
+++

<style type="text/css" media="all">
  a.supportlink{
    border-radius: var(--bs-border-radius) !important;
    padding: 0.5rem !important;
    --bs-border-width: 1px;
    border: 1px var(--bs-border-style) var(--bs-border-color) !important;
    display: inline-block !important;
    cursor: pointer;
    animation: 2s infinite ease-in-out floating;
  }

  a.supportlink.disabled{
    cursor: unset;
    animation: none !important;
  }
  a.supportlink.disabled i{
    color:grey !important;
  }

  .floating{
    animation: 2s infinite ease-in-out floating;
  }

  @keyframes floating {
    0% {
      transform: translatey(0);
    }
    50% {
      transform: translatey(-2px);
    }
    100% {
      transform: translatey(0);
    }
  }

  table th{
    text-align: center;
    font-size: 1.5rem;
  }
  
  #lessonBlocHTML-content > div#ctable{
    margin-left: -0.5rem;
  }
  
  #lessonBlocHTML-content > div#ctable > table{
    border-collapse: separate;
    border-spacing: 0.5rem 1rem;
    border: none !important;
  }
  
  #lessonBlocHTML-content > div#ctable > table > thead > tr > th{
    border:0 !important;
    text-align: left;
  }
  
  #lessonBlocHTML-content > div#ctable > table > tbody > tr{
    border-bottom: 1px solid black;
  }
  
  #lessonBlocHTML-content > div#ctable > table > tbody > tr > td:nth-child(2){
    font-weight: bold;
    width: 1%;
    text-align: center;
    vertical-align: middle;
  }
  
  #lessonBlocHTML-content > div#ctable > table > tbody > tr > td:nth-child(3){
    display: inline-table;
    border: 0 !important;
  }
  
  table tr td.done:first-child{
    width: 15px;
    background-color: #00ff11;
    color:#00ff11;
    height: 100%;
  }
  table table{
    border-collapse: separate;
    border-spacing: 0.5rem 0rem;
    border: none !important;
    width: min-content !important;
  }

  table table td{
    font-weight: normal !important;
    text-align: left !important;
    padding: 0 1rem 0 0.2rem !important;
    white-space:nowrap;
  }
  
  table table th{
    font-size: 1rem;
    vertical-align: middle;
    white-space:nowrap;
    width:min-content;
  }
  table table th code{
    white-space: nowrap;
  }
  table table ul{
    padding-left: 1rem !important;
    margin-bottom: 0;
    list-style-type:'- ';
  }
</style>
  